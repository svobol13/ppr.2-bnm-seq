package cz.svobol.fit.ppr2.barveniMrizky;


/**
 * Class represents Edge. It is used for purpose of indexing edges of
 * unknown color.
 * 
 * @author Lukáš Svoboda
 */
final class Edge {

	final int x;
	final int y;
	final boolean vertical;

	public Edge(int x, int y, boolean vertical) {
		this.x = x;
		this.y = y;
		this.vertical = vertical;
	}

	@Override
	public String toString() {
		return "Edge [x=" + x + ", y=" + y + ", vertical=" + vertical
				+ ", hashCode()=" + hashCode() + "]";
	}

	public static int hashCode(int x, int y, boolean vertical) {
		int hash = x * 10000;
		hash += y * 10;
		hash += vertical ? 1 : 9;
		return hash;
	}

	@Override
	public int hashCode() {
		return hashCode(x, y, vertical);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this)
			return true;
		if (obj instanceof Edge) {
			Edge idx = (Edge) obj;
			return idx.vertical == this.vertical && idx.x == this.x
					&& idx.y == this.y;
		}
		return false;
	}

}