package cz.svobol.fit.ppr2.barveniMrizky;

public class Printer {
	
	private static final String SPACE = "     ";

	public static void print(byte[][][] graph) {
		for (int y = 0; y < graph.length; y++) {
			
			StringBuilder horizontal = new StringBuilder();
			StringBuilder vert1 = new StringBuilder();
			StringBuilder vert2 = new StringBuilder();
			
			for (int x = 0; x < graph[0].length; x++) {
				byte hor = graph[y][x][1];
				byte ver = graph[y][x][0];
				
				if ((horizontal.length() > 0 && horizontal.charAt(horizontal.length()-1) != 'o') || horizontal.length() == 0)
					horizontal.append(" ");
				
				if (hor != -1) {
					horizontal.append("--"+eval(hor)+"--");
				} else {
					horizontal.append(SPACE);
				}
				
				String eval = eval(ver);
				if (eval != " ")
					vert1.append("|");
				else 
					vert1.append(" ");
				vert1.append(SPACE);
				vert2.append(eval);
				vert2.append(SPACE);
			}
			
			System.out.println(horizontal.toString());
			System.out.println(vert1);
			System.out.println(vert2.toString());
			System.out.println(vert1);
		}
	}
	
	private static String eval(byte b) {
		String val = "";
		if (b == -1)
			val = " ";
		else if (b == 0)
			val = "?";
		else 
			val = b+"";
		return val;
	}
}
