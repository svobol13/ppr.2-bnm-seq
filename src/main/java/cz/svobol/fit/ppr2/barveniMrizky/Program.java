package cz.svobol.fit.ppr2.barveniMrizky;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;


public class Program {

	// No edge variable
	private static final byte $ = -1;
	// Edge without color
	private static final byte _ = 0;
	// Highest color to be used
	private static final byte MAX_VALUE = Byte.MAX_VALUE;
	
	/**
	 * [*][ ][ ] - y <0,Ymax> position
	 * [ ][*][ ] - x <0,Xmax> position
	 * [ ][ ][*] - edge <0,1> : 0 - vertical; 1 - horizontal
	 */
//	private static final byte GRAPH[][][] = {
//		// Whole graph is composed of atomic values.
//		// Atomic value can be graphicaly represented as 
//		// a point with edge to the right and down:
//		//
//		//    o--
//		//    |
//		// 
//		// o   is source node represented by position in array [x, y]
//		// --  is horizontal edge represented by [y][x][1] byte value
//		// |   is vertical edge represented by   [y][x][0] byte value
//		{{1, _}, {2, _}, {4, $}, {$, $}},
//		{{2, _}, {1, _}, {_, 4}, {_, $}},
//		{{1, _}, {2, 2}, {$, 3}, {$, $}},
//		{{$, 3}, {$, $}, {$, $}, {$, $}} 
//	};
	
	private static final byte GRAPH[][][] = {
		{{1, 2}, {3, $}, {$, $}, {$, $}},
		{{2, 4}, {_, _}, {4, 3}, {_, $}},
		{{$, _}, {_, 3}, {_, 1}, {_, $}},
		{{$, $}, {$, _}, {$, _}, {$, $}} 
	};
	
	private static final Edge[] EDGE_ARRAY;
	private static final Map<Integer, Integer> EDGE_INDEX;
	
	static {
		List<Edge> edges = new LinkedList<>();
		Map<Integer, Integer> indices = new HashMap<>();
		
		for (int x = 0; x < GRAPH.length; x++) {
			for (int y = 0; y < GRAPH[0].length; y++) {
				byte[] node = GRAPH[y][x];
				
				// Vertical edge is unknown
				if (isUnknown(node[0])) {
					Edge e = new Edge(x, y, true);
					indices.put(e.hashCode(), edges.size());
					edges.add(e);
				}
				
				// Horizontal edge is unknown
				if (isUnknown(node[1])) {
					Edge e = new Edge(x, y, false);
					indices.put(e.hashCode(), edges.size());
					edges.add(e);
				}
				
			}
		}
		EDGE_ARRAY = edges.toArray(new Edge[]{});
		EDGE_INDEX = indices;
	}
	
	public static void main(String[] args) {
		long start = new Date().getTime();
		long bestScore = Long.MAX_VALUE;
		Byte[] bestSolution = null;
		Stack<Byte> stack = new Stack<>();
		
	 	byte edgeColor = 1;
		long tempSum = 0;
		while(true) {
			// Back track or terminate
			int size = stack.size();
			boolean finished = size == EDGE_ARRAY.length;
			if (edgeColor > MAX_VALUE || bestScore < tempSum + edgeColor || finished) {
				// Solution found
				if (finished) {
					bestSolution = stack.toArray(new Byte[]{});
					bestScore = tempSum;
				}
				if (size > 0) {
					Byte pop = stack.pop();
					tempSum -= pop;
					edgeColor = ++pop;
					continue;
				}
				break;
			}
			
			boolean valid = validInsert(edgeColor, size, stack.toArray(new Byte[EDGE_ARRAY.length]));

			if (valid) {
				stack.push(edgeColor);
				tempSum += edgeColor;
				edgeColor = 1;
				continue;
			}
			
			edgeColor++;
		}
		long stop = new Date().getTime();
		
		System.out.println("\n\n\nUnsolved problem.");
		Printer.print(GRAPH);
		System.out.println("Solved problem. \nPrice: "+bestScore+" \nSolution vector: "+Arrays.deepToString(bestSolution));
		System.out.println("Time: " + (stop - start) + "ms");
		Printer.print(merge(GRAPH, bestSolution));
	}

	/**
	 * Determine whether insertion of the given color into 
	 * the given index is valid operation with respect to 
	 * already inserted colors.
	 * @param edgeColor Tested 'color' to be inserted
	 * @param vectorEdgeIndex Index of 'edge'.
	 * @param partialSolutionVector Array of 'colorized' edges.
	 * @return
	 */
	private static boolean validInsert(byte edgeColor, int vectorEdgeIndex,
			Byte[] partialSolutionVector) {
		Edge edge = EDGE_ARRAY[vectorEdgeIndex];
		try {
			if (edge.vertical) {
				checkEdgeColor(edge.x+0, edge.y+0, false, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+0, edge.y+1, false, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x-1, edge.y+0, false, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x-1, edge.y+1, false, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x-1, edge.y+0, true, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+1, edge.y+0, true, partialSolutionVector, edgeColor);
			} else {
				checkEdgeColor(edge.x+0, edge.y+0, true, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+0, edge.y-1, true, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+1, edge.y-1, true, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+1, edge.y+0, true, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+0, edge.y+1, false, partialSolutionVector, edgeColor);
				checkEdgeColor(edge.x+0, edge.y-1, false, partialSolutionVector, edgeColor);
			}
		} catch (AssertionError e) {
			return false;
		}
		return true;
	}


	/**
	 * Checks whether given edge[x,y,vertical] is of same color
	 * as the given edgeColor argument. It it is then throws an
	 * exception.
	 */
	private static void checkEdgeColor(int x, int y, boolean vertical, 
			Byte[] partialSolutionVector, byte edgeColor) {
		byte[] node;
		// Get node from graph
		try {
			node = GRAPH[y][x];
		} catch (IndexOutOfBoundsException e) {
			return;
		}
		// There is no node on the given coords
		if (node == null) {
			return;
		}
		// Get node's edge
		Byte edge = node[vertical?0:1];
		// Edge is of unknown color check if it is 
		// not already set in solution vector.
		if (edge == _) {
			Integer vectorIndex = EDGE_INDEX.get(Edge.hashCode(x, y, vertical));
			edge = partialSolutionVector[vectorIndex];
		}
		// No conflict found, continue.
		if (edge == null || edge != edgeColor) {
			return;
		}
		throw new AssertionError();
	}

	/**
	 * @param edgeColor May be <code>null</code>.
	 * @return <code>true</code> if edge has unknown color ({@code 0x0}). 
	 * Otherwise <code>false</code>.
	 */
	private static boolean isUnknown(byte edgeColor) {
		return edgeColor == _; 
	}
	
	private static byte[][][] merge(byte[][][] graph, Byte[] solution) {
		byte[][][] merged = graph.clone();
		for (int i = 0; i < solution.length; i++) {
			Edge edge = EDGE_ARRAY[i];
			merged[edge.y][edge.x][edge.vertical?0:1] = solution[i];
		}
		return merged;
	}
	
}
